package eu.kanade.tachiyomi.source.customs.MMRCMS

class Frscan : CommonMMRCMS() {

    override val id: Long = 10

    override val name = "FR scan"

    override val baseUrl = "https://frscan.ws/"
}
