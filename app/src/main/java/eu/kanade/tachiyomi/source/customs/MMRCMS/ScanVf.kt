package eu.kanade.tachiyomi.source.customs.MMRCMS

class ScanVf : CommonMMRCMS() {

    override val id: Long = 13

    override val name = "Scan VF"

    override val baseUrl = "https://www.scan-vf.net/"
}
