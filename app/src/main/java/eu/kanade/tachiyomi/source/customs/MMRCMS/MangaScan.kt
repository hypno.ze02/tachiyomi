package eu.kanade.tachiyomi.source.customs.MMRCMS

class MangaScan : CommonMMRCMS() {

    override val id: Long = 11

    override val name = "Manga Scan"

    override val baseUrl = "https://manga-scan.ws/"
}
