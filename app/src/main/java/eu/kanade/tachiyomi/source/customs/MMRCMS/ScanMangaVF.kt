package eu.kanade.tachiyomi.source.customs.MMRCMS

class ScanMangaVF : CommonMMRCMS() {

    override val id: Long = 12

    override val name = "ScanManga VF"

    override val baseUrl = "https://scanmanga-vf.ws/"
}
