package eu.kanade.tachiyomi.source.customs.toscrap

import android.app.Application
import android.content.SharedPreferences
import android.net.Uri
import eu.kanade.tachiyomi.network.GET
import eu.kanade.tachiyomi.network.interceptor.rateLimit
import eu.kanade.tachiyomi.source.ConfigurableSource
import eu.kanade.tachiyomi.source.model.Filter
import eu.kanade.tachiyomi.source.model.FilterList
import eu.kanade.tachiyomi.source.model.MangasPage
import eu.kanade.tachiyomi.source.model.Page
import eu.kanade.tachiyomi.source.model.SChapter
import eu.kanade.tachiyomi.source.model.SManga
import eu.kanade.tachiyomi.source.online.ParsedHttpSource
import eu.kanade.tachiyomi.util.asJsoup
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import okhttp3.Headers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import uy.kohesive.injekt.injectLazy
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Papscan : ConfigurableSource, ParsedHttpSource() {

    // A SCRAP DIFFEREMMENT

    override val id: Long = 22

    override val name = "Scan Manga"

    override val baseUrl = "https://scansmangas.ws/"

    override val lang = "fr"

    override val supportsLatest = true

    private val json: Json by injectLazy()

    private val preferences: SharedPreferences by lazy {
        Injekt.get<Application>().getSharedPreferences("source_$id", 0x0000)
    }

    override val client: OkHttpClient = network.client.newBuilder()
        .rateLimit(1, 2)
        .build()

    private val parsedBaseUrl = Uri.parse(baseUrl)

    companion object {
        val dateFormat by lazy {
            SimpleDateFormat("dd MMM. yyyy", Locale.FRANCE)
        }
        private const val SHOW_SPOILER_CHAPTERS_Title = "Les chapitres en Anglais ou non traduit sont upload en tant que \" Spoilers \" sur Japscan"
        private const val SHOW_SPOILER_CHAPTERS = "JAPSCAN_SPOILER_CHAPTERS"
        private val prefsEntries = arrayOf("Montrer uniquement les chapitres traduit en Français", "Montrer les chapitres spoiler")
        private val prefsEntryValues = arrayOf("hide", "show")
    }

    private fun chapterListPref() = preferences.getString(SHOW_SPOILER_CHAPTERS, "hide")

    // Popular
    override fun popularMangaRequest(page: Int): Request {
        return GET("$baseUrl/filterList?page=$page&sortBy=views&asc=false", headers)
    }

    override fun popularMangaParse(response: Response) = internalMangaParse(response)

    private fun internalMangaParse(response: Response): MangasPage {
        val document = response.asJsoup()

        val internalMangaSelector = when (name) {
            "Utsukushii" -> "div.content div.col-sm-6"
            else -> "div[class^=col-sm], div.col-xs-6"
        }
        return MangasPage(
            document.select(internalMangaSelector).map {
                SManga.create().apply {
                    val urlElement = it.getElementsByClass("chart-title")
                    if (urlElement.size == 0) {
                        url = getUrlWithoutBaseUrl(it.select("a").attr("href"))
                        title = it.select("div.caption").text()
                        it.select("div.caption div").text().let { if (it.isNotEmpty()) title = title.substringBefore(it) } // To clean submanga's titles without breaking hentaishark's
                    } else {
                        url = getUrlWithoutBaseUrl(urlElement.attr("href"))
                        title = urlElement.text().trim()
                    }

                    it.select("img").let { img ->
                        thumbnail_url = when {
                            it.hasAttr("data-background-image") -> it.attr("data-background-image") // Utsukushii
                            img.hasAttr("data-src") -> coverGuess(img.attr("abs:data-src"), url)
                            else -> coverGuess(img.attr("abs:src"), url)
                        }
                    }
                }
            },
            document.select(".pagination a[rel=next]").isNotEmpty(),
        )
    }

    // Guess thumbnails on broken websites
    fun coverGuess(url: String?, mangaUrl: String): String? {
        return if (url?.endsWith("no-image.png") == true) {
            "$baseUrl/uploads/manga/${mangaUrl.substringAfterLast('/')}/cover/cover_250x350.jpg"
        } else {
            url
        }
    }

    override fun popularMangaNextPageSelector(): String? = null

    override fun popularMangaSelector() = ".hot-thumbnails .span3 .photo .thumbnail"

    override fun popularMangaFromElement(element: Element): SManga {
        val manga = SManga.create()
        element.select("a").first()!!.let { a ->
            val chapterUrl = a.attr("href")
            val chapterNo = chapterUrl.split("/").last()
            manga.setUrlWithoutDomain(chapterUrl.replace(chapterNo, ""))
            a.select("img").first()!!.let { img ->
                manga.title = img.text()
                manga.thumbnail_url = "https:" + img.attr("src")
            }
        }
        return manga
    }

    private val latestTitles = mutableSetOf<String>()

    override fun latestUpdatesParse(response: Response): MangasPage {
        val document = response.asJsoup()

        if (document.location().contains("page=1")) latestTitles.clear()

        val mangas = document.select(latestUpdatesSelector())
            .let { elements ->
                when {
                    // List layout (most sources)
                    elements.select("a").firstOrNull()?.hasText() == true -> elements.map { latestUpdatesFromElement(it, "a") }
                    // Grid layout (e.g. MangaID)
                    else -> document.select(gridLatestUpdatesSelector()).map { gridLatestUpdatesFromElement(it) }
                }
            }
            .filterNotNull()

        return MangasPage(mangas, document.select(latestUpdatesNextPageSelector()) != null)
    }

    override fun latestUpdatesSelector() = "div.mangalist div.manga-item"
    override fun latestUpdatesNextPageSelector() = "a[rel=next]"

    override fun latestUpdatesFromElement(element: Element, urlSelector: String): SManga {
        return element.select(urlSelector).first()!!.let { titleElement ->
            if (titleElement.text() in latestTitles) {
                null
            } else {
                latestTitles.add(titleElement.text())
                SManga.create().apply {
                    url = titleElement.attr("abs:href").substringAfter(baseUrl) // intentionally not using setUrlWithoutDomain
                    title = titleElement.text().trim()
                    thumbnail_url = "$baseUrl/uploads/manga/${url.substringAfterLast('/')}/cover/cover_250x350.jpg"
                }
            }!!
        }
    }

    private fun gridLatestUpdatesSelector() = "div.mangalist div.manga-item, div.grid-manga tr"
    protected open fun gridLatestUpdatesFromElement(element: Element): SManga = SManga.create().apply {
        element.select("a.chart-title").let {
            setUrlWithoutDomain(it.attr("href"))
            title = it.text()
        }
        thumbnail_url = element.select("img").attr("abs:src")
    }

    // Search
    override fun searchMangaRequest(page: Int, query: String, filters: FilterList): Request {
        if (query.isEmpty()) {
            val uri = Uri.parse(baseUrl).buildUpon()
                .appendPath("search?query=")
            return GET(uri.toString(), headers)
        } else {
            val searchHeaders = headers.newBuilder()
                .add("X-Requested-With", "XMLHttpRequest")
                .build()

            val searchRequest = GET("$baseUrl/search?query=$query", searchHeaders)
            val searchResponse = client.newCall(searchRequest).execute()

            if (!searchResponse.isSuccessful) {
                throw Exception("Code ${searchResponse.code} inattendu")
            }

            val jsonResult = json.parseToJsonElement(searchResponse.body.string()).jsonObject["suggestions"]!!.jsonArray

            if (jsonResult.isEmpty()) {
                throw Exception("Pas de données")
            }

            return searchRequest
        }
    }

    override fun latestUpdatesRequest(page: Int) = GET("$baseUrl/latest-release?page=$page", headers)

    override fun searchMangaNextPageSelector(): String? = null

    override fun searchMangaSelector(): String = ""

    override fun searchMangaParse(response: Response): MangasPage {
        if ("search" in response.request.url.toString()) {
            val jsonResult = json.parseToJsonElement(response.body.string()).jsonObject["suggestions"]!!.jsonArray

            val mangaList = jsonResult.map { jsonEl -> searchMangaFromJson(jsonEl.jsonObject) }

            return MangasPage(mangaList, hasNextPage = false)
        }

        return super.searchMangaParse(response)
    }

    override fun searchMangaFromElement(element: Element): SManga {
        return if (element.attr("class") == "result-link") {
            SManga.create().apply {
                title = element.text().substringAfter(" ").substringBefore(" | JapScan")
                setUrlWithoutDomain(element.attr("abs:href"))
            }
        } else {
            SManga.create().apply {
                thumbnail_url = element.select("img").attr("abs:src")
                element.select("p a").let {
                    title = it.text()
                    url = it.attr("href")
                }
            }
        }
    }

    private fun searchMangaFromJson(jsonObj: JsonObject): SManga = SManga.create().apply {
        title = jsonObj["value"]!!.jsonPrimitive.content
        url = "manga/" + jsonObj["data"]!!.jsonPrimitive.content
    }

    override fun mangaDetailsParse(document: Document): SManga {
        val infoElement = document.selectFirst(".container > .row > .col-sm-12 > .row")!!

        val manga = SManga.create()
        manga.thumbnail_url = infoElement.select("img").attr("abs:src")

        val infoContainer = infoElement.select(".dl-horizontal")
        val titleRows = infoContainer.select("dt")
        val valueRows = infoContainer.select("dd")
        titleRows.select("dt").forEachIndexed { index, el ->
            when (el.text().trim()) {
                "Statut" -> manga.status = valueRows[index].select("span").text().trim().let {
                    parseStatus(it)
                }

                "Auteur(s)" -> manga.author = valueRows[index].select("a").text().trim()
                "Artist(s)" -> manga.artist = valueRows[index].select("a").text().trim()
                "Catégories" -> manga.genre = valueRows[index].select("a").map { categorie ->
                    categorie.text()
                }.joinToString { s: String -> s }
            }
        }
        manga.description = document.select(".row .well > p").text()
        manga.title = document.select(".widget-title").first()!!.text()

        return manga
    }

    private fun parseStatus(status: String) = when {
        status.contains("Ongoing") -> SManga.ONGOING
        status.contains("Complete") -> SManga.COMPLETED
        else -> SManga.UNKNOWN
    }

    override fun chapterListSelector() = ".chapters > li:not(.btn)"

    override fun chapterFromElement(element: Element): SChapter {
        val chapter = SChapter.create()
        try {
            val urlElement = element.selectFirst(".chapter-title-rtl > a")!!
            val nameElement = element.selectFirst(".chapter-title-rtl > em")!!
            val dateElement = element.selectFirst(".action > .date-chapter-title-rtl")!!

            chapter.setUrlWithoutDomain(urlElement.attr("href"))
            chapter.name = "${urlElement.text()} : ${nameElement.text()}"
            chapter.date_upload = dateElement.text().trim().let { parseChapterDate(it) }
        } catch (e: NullPointerException) {
            return chapter
        }
        return chapter
    }

    private fun parseChapterDate(date: String): Long {
        return try {
            dateFormat.parse(date)?.time ?: 0
        } catch (e: ParseException) {
            0L
        }
    }

    override fun pageListParse(document: Document): List<Page> {
        val pageListElement = document.select("div#all")
        val pagesElement = pageListElement.select("img")
        if (!pagesElement.isEmpty()) {
            return pagesElement.mapIndexed { index, page ->
                Page(index, imageUrl = "https:${page.attr("data-src").trim()}".replace(" ", ""))
            }
        }

        throw Exception("Impossible de récupérer les pages")
    }

    override fun headersBuilder(): Headers.Builder {
        return Headers.Builder().apply {
            add("User-Agent", network.defaultUserAgent)
            add("Keep-Alive", "true")
        }
    }

    override fun imageRequest(page: Page): Request {
        return GET(page.imageUrl!!, headers)
    }

    override fun imageUrlParse(document: Document): String {
        return ""
    }

    // Filters
    private class TextField(name: String) : Filter.Text(name)

    private class PageList(pages: Array<Int>) : Filter.Select<Int>("Page #", arrayOf(0, *pages))

    override fun getFilterList(): FilterList {
        val totalPages = pageNumberDoc?.select("li.page-item:last-child a")?.text()
        val pagelist = mutableListOf<Int>()
        return if (!totalPages.isNullOrEmpty()) {
            for (i in 0 until totalPages.toInt()) {
                pagelist.add(i + 1)
            }
            FilterList(
                Filter.Header("Page alphabétique"),
                PageList(pagelist.toTypedArray()),
            )
        } else {
            FilterList(
                Filter.Header("Page alphabétique"),
                TextField("Page #"),
                Filter.Header("Appuyez sur reset pour la liste"),
            )
        }
    }

    private var pageNumberDoc: Document? = null

    // Prefs
    override fun setupPreferenceScreen(screen: androidx.preference.PreferenceScreen) {
        val chapterListPref = androidx.preference.ListPreference(screen.context).apply {
            key = SHOW_SPOILER_CHAPTERS_Title
            title = SHOW_SPOILER_CHAPTERS_Title
            entries = prefsEntries
            entryValues = prefsEntryValues
            summary = "%s"

            setOnPreferenceChangeListener { _, newValue ->
                val selected = newValue as String
                val index = this.findIndexOfValue(selected)
                val entry = entryValues[index] as String
                preferences.edit().putString(SHOW_SPOILER_CHAPTERS, entry).commit()
            }
        }
        screen.addPreference(chapterListPref)
    }

    fun getUrlWithoutBaseUrl(newUrl: String): String {
        val parsedNewUrl = Uri.parse(newUrl)
        val newPathSegments = parsedNewUrl.pathSegments.toMutableList()

        for (i in parsedBaseUrl.pathSegments) {
            if (i.trim().equals(newPathSegments.first(), true)) {
                newPathSegments.removeAt(0)
            } else {
                break
            }
        }

        val builtUrl = parsedNewUrl.buildUpon().path("/")
        newPathSegments.forEach { builtUrl.appendPath(it) }

        var out = builtUrl.build().encodedPath!!
        if (parsedNewUrl.encodedQuery != null) {
            out += "?" + parsedNewUrl.encodedQuery
        }
        if (parsedNewUrl.encodedFragment != null) {
            out += "#" + parsedNewUrl.encodedFragment
        }

        return out
    }
}
